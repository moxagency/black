<?php

// Name of the coffee
add_action("espresso2", "espresso2");

function espresso2() {

    $espresso2 = get_option('espresso2', 'hello world');
    echo $espresso2;
}

// Roastery
add_action("espresso2_roastery", "espresso2_roastery");

function espresso2_roastery() {

    $espresso2_roastery = get_option('espresso2_roastery', 'hello world');
    echo $espresso2_roastery;
}

// Tasting profile
add_action("espresso2_tastingprofile", "espresso2_tastingprofile");

function espresso2_tastingprofile() {

    $espresso2_tastingprofile = get_option('espresso2_tastingprofile', 'hello world');
    echo $espresso2_tastingprofile;
}