<?php

// Name of the coffee
add_action("espresso1", "espresso1");

function espresso1() {

    $espresso1 = get_option('espresso1', 'hello world');
    echo $espresso1;
}

// Roastery
add_action("espresso1_roastery", "espresso1_roastery");

function espresso1_roastery() {

    $espresso1_roastery = get_option('espresso1_roastery', 'hello world');
    echo $espresso1_roastery;
}

// Tasting profile
add_action("espresso1_tastingprofile", "espresso1_tastingprofile");

function espresso1_tastingprofile() {

    $espresso1_tastingprofile = get_option('espresso1_tastingprofile', 'hello world');
    echo $espresso1_tastingprofile;
}