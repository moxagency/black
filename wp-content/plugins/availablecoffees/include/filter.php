<?php

// Name of the coffee
add_action("filter", "filter");

function filter() {

    $filter = get_option('filter', 'hello world');
    echo $filter;
}

// Roastery
add_action("filter_roastery", "filter_roastery");

function filter_roastery() {

    $filter_roastery = get_option('filter_roastery', 'hello world');
    echo $filter_roastery;
}

// Tasting profile
add_action("filter_tastingprofile", "filter_tastingprofile");

function filter_tastingprofile() {

    $filter_tastingprofile = get_option('filter_tastingprofile', 'hello world');
    echo $filter_tastingprofile;
}