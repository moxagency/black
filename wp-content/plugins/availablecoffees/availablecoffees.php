<?php
/*
Plugin Name: Available Coffees
Author: Junk'n Gold
Version: 1.1
*/

require_once 'include/espresso1.php';
require_once 'include/espresso2.php';
require_once 'include/filter.php';




function my_admin_menu () {
    add_menu_page('Available Coffees', 'Kávy', 'manage_options', __FILE__, 'coffee_change');
}

add_action('admin_menu', 'my_admin_menu');

function coffee_change () {

    if (isset($_POST['change-clicked'])) {
        // Espresso 1
        update_option( 'espresso1', $_POST['espresso1'] );
        $espresso1 = get_option('espresso1', 'Name of the coffee');

        update_option( 'espresso1_roastery', $_POST['espresso1_roastery'] );
        $espresso1_roastery = get_option('espresso1_roastery', 'Roastery');

        update_option( 'espresso1_tastingprofile', $_POST['espresso1_tastingprofile'] );
        $espresso1_tastingprofile = get_option('espresso1_tastingprofile', 'Tasting profile');

        // Espresso 2
        update_option( 'espresso2', $_POST['espresso2'] );
        $espresso2 = get_option('espresso2', 'Name of the coffee');

        update_option( 'espresso2_roastery', $_POST['espresso2_roastery'] );
        $espresso2_roastery = get_option('espresso2_roastery', 'Roastery');

        update_option( 'espresso2_tastingprofile', $_POST['espresso2_tastingprofile'] );
        $espresso2_tastingprofile = get_option('espresso2_tastingprofile', 'Tasting profile');

        // Filter
        update_option( 'filter', $_POST['filter'] );
        $filter = get_option('filter', 'Name of the coffee');

        update_option( 'filter_roastery', $_POST['filter_roastery'] );
        $filter_roastery = get_option('filter_roastery', 'Roastery');

        update_option( 'filter_tastingprofile', $_POST['filter_tastingprofile'] );
        $filter_tastingprofile = get_option('filter_tastingprofile', 'Tasting profile');
    }

    ?>

    <div class="wrap">
        <h1>Dostupné kávy</h1>

        <form action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="post">
            <h3>Espresso 1:</h3>

            Espresso 1:<input type="text" value="<?php do_action( 'espresso1' ); ?>" name="espresso1" placeholder="hello world"><br />
            Espresso 1 roastery:<input type="text" value="<?php do_action( 'espresso1_roastery' ); ?>" name="espresso1_roastery" placeholder="hello world"><br />
            Espresso 1 tasting profile:<input type="text" value="<?php do_action( 'espresso1_tastingprofile' ); ?>" name="espresso1_tastingprofile" placeholder="hello world"><br />

            <h3>Espresso 2:</h3>

            Espresso 2:<input type="text" value="<?php do_action( 'espresso2' ); ?>" name="espresso2" placeholder="hello world"><br />
            Espresso 2 roastery:<input type="text" value="<?php do_action( 'espresso2_roastery' ); ?>" name="espresso2_roastery" placeholder="hello world"><br />
            Espresso 2 tasting profile:<input type="text" value="<?php do_action( 'espresso2_tastingprofile' ); ?>" name="espresso2_tastingprofile" placeholder="hello world"><br />

            <h3>Filter:</h3>

            Filter:<input type="text" value="<?php do_action( 'filter' ); ?>" name="filter" placeholder="hello world"><br />
            Filter roastery:<input type="text" value="<?php do_action( 'filter_roastery' ); ?>" name="filter_roastery" placeholder="hello world"><br />
            Filter tasting profile:<input type="text" value="<?php do_action( 'filter_tastingprofile' ); ?>" name="filter_tastingprofile" placeholder="hello world"><br />

            <input name="change-clicked" type="hidden" value="1" />
            <input type="submit" value="Zmeniť" />
        </form>
    </div>

    <?

}