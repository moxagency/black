<?php
include_once dirname(__FILE__) . '/lib/BesteronButtonsClass.php';

class WC_Besteron_Buttons_Payment_Gateway extends WC_Payment_Gateway {


    // Setup our Gateway's id, description and other values
    function __construct() {

        // The global ID for this Payment method
        $this->id = "besteron_buttons";

        // The Title shown on the top of the Paymegnt Gateways Page next to all the other Payment Gateways
        $this->method_title = __("Online Buttons Payment Gateway", 'besteron-buttons-payment-gateway');

        // The description for this Payment Gateway, shown on the actual Payment options page on the backend
        $this->method_description = __("Online Buttons Payment Gateway Plug-in for WooCommerce", 'besteron-buttons-payment-gateway');

        // The title to be used for the vertical tabs that can be ordered top to bottom
        $this->title = __('Online Buttons Payment Gateway', 'besteron-buttons-payment-gateway');

        // If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
        $this->icon = null;

        // Bool. Can be set to true if you want payment fields to show on the checkout
        // if doing a direct integration, which we are doing in this case
        $this->has_fields = true;

        // Supports the default credit card form
        $this->supports = array();

        // This basically defines your settings which are then loaded with init_settings()
        $this->init_form_fields();

        // After init_settings() is called, you can get the settings and load them into variables, e.g:
        // $this->title = $this->get_option( 'title' );
        $this->init_settings();

        // Turn these settings into variables we can use
        foreach ($this->settings as $setting_key => $value) {
            $this->$setting_key = $value;
        }

        // Lets check for SSL
        //add_action('admin_notices', array($this, 'do_ssl_check'));
        // Save settings
        if (is_admin()) {
            // Versions over 2.0
            // Save our administration options. Since we are not going to be doing anything special
            // we have not defined 'process_admin_options' in this class so the method in the parent
            // class will be used instead
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        }
        //TODO: pozri to
        add_action('woocommerce_thankyou_' . $this->id, array(&$this, 'thankyou_page'));
        //add_action('woocommerce_receipt_'.$this->id, array(&$this, 'receipt_page'));
    }

//    /**
//     * Receipt Page
//     *
//     */
//    function receipt_page($order_id) {
//        // echo '<pre>order='. print_r($order, true).'</pre>'; exit;
//    }

    /**
     * Thank you Page
     *
     */
    function thankyou_page($order_id) {
        global $woocommerce;

        $order = new WC_Order($order_id);

        //check is result
        $result = array_key_exists(BesteronButtons::PARAM_RESULT, $_REQUEST) ? $_REQUEST[BesteronButtons::PARAM_RESULT] : null;
        if (!$result) {
            return;
        }

        //vlidate sign
        $vs = str_pad($order_id, 10, "0", STR_PAD_LEFT);
        $error = false;
        if (!$error && $vs != $_REQUEST[BesteronButtons::PARAM_VS]) {
            $error = "nesuhlasi vs ";
        }
        if (!$error && $order->order_total != $_REQUEST[BesteronButtons::PARAM_AMNT]) {
            $error = "nesuhlasi amnt";
        }
        if (!$error && '978' != $_REQUEST[BesteronButtons::PARAM_CURR]) {
            $error = "nesuhlasi curr";
        }
        if (!$error) {
            if ($result == BesteronButtons::RESULT_OK|| $result == BesteronButtons::RESULT_FAIL) {
                //validate sign
                try {
                    $besteron_demo = ( $this->demo == "yes" ) ? true : false;
                    $besteron = new BesteronButtonsResponse(
                            array(
                                BesteronButtons::PARAM_CID => trim($this->besteron_cid),
                                BesteronButtons::PARAM_KEY => trim($this->besteron_key),
                                BesteronButtons::PARAM_DEMO_MODE => $besteron_demo,
                                BesteronButtons::PARAM_AMNT   => $_REQUEST['AMNT'],
                                BesteronButtons::PARAM_CURR   => BesteronButtons::CURR_EURO,
                                BesteronButtons::PARAM_VS     => $_REQUEST['VS'],
                                BesteronButtons::PARAM_TYPE   => $_REQUEST['TYPE'],
                                BesteronButtons::PARAM_RESULT => $result,
                                BesteronButtons::PARAM_SIGN   => $_REQUEST['SIGN'],
                            )
                    );
                    if (!$besteron->isValid()) {
                        $error = 'invalid sign';
                    }
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
            } else {
                $error = __('Unknown server response:', 'besteron-buttons-payment-gateway').' ' . $result;
                $order->update_status('failed');
                $order->add_order_note(__('Payment transaction failed', 'besteron-buttons-payment-gateway') . ' ' . $error);
            }
        }
        if ($error) {
            $msg = __('An error occurred. The payment has not validated.', 'besteron-buttons-payment-gateway') . 'error: ' . $error;
            wc_add_notice($msg, 'error');

            $redirect_url = $this->get_return_url($order);
            $this->web_redirect($redirect_url);
            exit;

        }
        if ($result == 'OK') {
            $order->payment_complete();
            $woocommerce->cart->empty_cart();
            $redirect_url = $this->get_return_url($order);
            $this->web_redirect($redirect_url);
        } else if ($result == BesteronButtons::RESULT_FAIL) {
            $msg = __('Payment transaction failed', 'besteron-buttons-payment-gateway');
            $order->update_status('failed');
            $order->add_order_note($msg);
            wc_add_notice($msg, 'error');
            $redirect_url = $this->get_return_url($order);
            $this->web_redirect($redirect_url);
            exit;
        }
    }

    public function web_redirect($url) {
        echo "<html><head><script language=\"javascript\">
                <!--
                window.location=\"{$url}\";
                //-->
                </script>
                </head><body><noscript><meta http-equiv=\"refresh\" content=\"0;url={$url}\"></noscript></body></html>";
    }

    // Build the administration fields for this specific Gateway
    public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable / Disable', 'besteron-buttons-payment-gateway'),
                'label' => __('Enable this payment gateway', 'besteron-buttons-payment-gateway'),
                'type' => 'checkbox',
                'default' => 'no',
            ),
            'demo' => array(
                'title' => __('Demo mode', 'besteron-buttons-payment-gateway'),
                'label' => __('Plugin is in demo mode', 'besteron-buttons-payment-gateway'),
                'type' => 'checkbox',
                'default' => 'yes',
            ),
            'besteron_cid' => array(
                'title' => __('CID', 'besteron-buttons-payment-gateway'),
                'type' => 'text',
                'desc_tip' => __('Your Merchant CID from your BESTERON account Integration page', 'besteron-buttons-payment-gateway'),
                'default' => '',
            ),
            'besteron_key' => array(
                'title' => __('KEY', 'besteron-buttons-payment-gateway'),
                'type' => 'password',
                'desc_tip' => __('Your Merchant Key from your BESTERON account Integration page', 'besteron-buttons-payment-gateway'),
            ),
        );
    }

    // Submit payment and handle response
    public function process_payment($order_id) {
        global $woocommerce;

        include_once dirname(__FILE__) . '/lib/BesteronButtonsClass.php';

        // Get this Order's information so that we know
        // who to charge and how much
        $order = new WC_Order($order_id);

        // Are we testing right now or is it a real transaction
        $besteron_demo = ( $this->demo == "yes" ) ? true : false;
        $vs = str_pad($order_id, 10, "0", STR_PAD_LEFT);
        $amnt = $order->order_total;

        //return on Order Received page
        $ru = $this->get_return_url($order);

        $bic = $_POST['besteron_bic'];
        try {
            $this->validBic($bic);
            $besteron = new BesteronButtonsRequest(
                    array(
                        BesteronButtons::PARAM_CID => trim($this->besteron_cid),
                        BesteronButtons::PARAM_KEY => trim($this->besteron_key),
                        BesteronButtons::PARAM_DEMO_MODE => $besteron_demo,
                        BesteronButtons::PARAM_AMNT => floatval($amnt),
                        BesteronButtons::PARAM_CURR => BesteronButtons::CURR_EURO,
                        BesteronButtons::PARAM_VS => $vs,
                        BesteronButtons::PARAM_RU => $ru,
                        BesteronButtons::PARAM_EMAIL => $order->billing_email,
                    )
            );
            $url = $besteron->getUrl($bic, $besteron::$BANKS_BUTTONS);
        } catch (Exception $e) {
            wc_add_notice($e->getMessage(), 'error');
            return false;
        }
//        // Save the payment token to the session for faster loading of the payment page
//        $woocommerce->session->paymentToken = array(
//
//        );
        return array(
            'result' => 'success',
            'redirect' => $url
        );
    }

    // Validate fields
    public function validate_fields() {

        return true;
    }

    // Check if we are forcing SSL on checkout pages
    // Custom function not required by the Gateway
    public function do_ssl_check() {
        if ($this->demo == "yes") {
            if (get_option('woocommerce_force_ssl_checkout') == "no") {
                echo "<div class=\"error\"><p>" . sprintf(__("<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>", 'besteron-buttons-payment-gateway'), $this->method_title, admin_url('admin.php?page=wc-settings&tab=checkout')) . "</p></div>";
            }
        }
    }

    public function payment_fields() {
        //parent::payment_fields();
        //$description = __('Please select your bank', 'besteron-buttons-payment-gateway');
        $description = __('Mediated by payment gateway', 'besteron-buttons-payment-gateway');
        wptexturize($description);

        // if (is_user_logged_in()) {
            $img_url =  plugins_url('assets/images/', __FILE__);
            $img_src = '3.png';

            echo '<div class="form-row form-row-wide">';

            echo "<div style='display:table; vertical-align: middle; line-height: 18px; height: 45px; float:left; padding:5px; text-align:left;'>"
                . "<span style='display:table-cell; vertical-align: middle; text-align: left;'>"
                . "{$description}"
                . "</span>"
                . "<div style=\"height: 45px; line-height: 40px; float:left; display:table-cell; vertical-align:middle;\">"
                . "<img src=\"{$img_url}besteron/besteron_logo_small.png\" alt=\"Besteron\" style=\"margin: auto 4px; vertical-align: middle; max-height:40px;\"/>"
                . "</div>"
                . "</div><br />";


            // echo $description . '<br/>';
            echo '<div style="clear: both;float:left;">';
            foreach (BesteronButtons::$BANKS_BUTTONS as $bic => $bank) {
                $radio_id = 'besteron_bic_' . $bic;
                //TODO: set checked
                $checked = false ? 'checked="checked"' : '';
                $radio_name = "besteron_bic";
                echo "<div style='display:table; vertical-align: middle; line-height: 18px; width:300px; height: 45px; float:left; padding:5px; text-align:center;'>"
                        . "<div style='display:table-cell; vertical-align: middle; width:10px;'>"
                        . "<input style=\"margin: 2 0 auto;\" class=\"besteron-bic\" type=\"radio\" id=\"$radio_id\" name=\"$radio_name\" value=\"{$bic}\" $checked />"
                        . "</div>"
                        . "<span style='display:table-cell; vertical-align: middle; width:90px; text-align: left;'>"
                        . "{$bank['label']}"
                        . "</span>"
                        . "<label for=\"$radio_id\">"
                        . "<div style=\"height: 45px; float:left; display:table-cell; vertical-align:middle;\">"
                        . "<img src=\"{$img_url}/{$bank['image']}/{$img_src}\" alt=\"{$bank['name']}\" style=\"margin: auto; vertical-align: middle; max-height:40px;\"/>"
                        . "</div>"
                        . "</label>"
                        . "</div><br />";
            }
            echo '</div></div>';
        // }
    }

    /*
     *

.hr-payment {
    float: left;
    margin: 15px 0 5px !important;
    width: 98%;
}
         */

    /**
     * validate BIC/SWIFT Code, url param TYPE
     *
     * @return boolean
     * @throws Exception
     */
    private function validBic($bic) {
        if (!$bic) {
            $msg = __('Besteron: Bank is not selected. Select please your bank.', 'besteron-buttons-payment-gateway');
            throw new Exception($msg);
        }
        if (!array_key_exists($bic, BesteronButtons::$BANKS_BUTTONS)) {
            $msg = __('Besteron: Unknown BIC/SWIFT', 'besteron-buttons-payment-gateway') . ': ' . $bic;
            ;
            throw new Exception($msg);
        }
        return true;
    }

}
