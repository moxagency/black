<?php

/*
  Plugin Name: Besteron Buttons Payment Gateway
  Plugin URI: http://www.besteron.sk/
  Description: Extends WooCommerce by Adding the Besteron Buttons Payment Gateway .
  Version: 1.0.8
  Author: Besteron
  Author URI: http://www.besteron.sk/
  License: GPLv2
*/
 
/*  Copyright (C) 2016 Besteron (email: it@besteron.sk)

	This program is free software; you can redistribute it and/or	
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/ 

// Include our Gateway Class and register Payment Gateway with WooCommerce
add_action('plugins_loaded', 'besteron_payment_buttons_init', 0);

function besteron_payment_buttons_init() {

    // If the parent WC_Payment_Gateway class doesn't exist
    // it means WooCommerce is not installed on the site
    // so do nothing
    if (!class_exists('WC_Payment_Gateway')) {
        return;
    }

    /**
     * Localisation
     */
    load_plugin_textdomain('besteron-buttons-payment-gateway', false, dirname(plugin_basename(__FILE__)) . '/languages');

    // If we made it this far, then include our Gateway Class
    include_once( 'class-wc-besteron-buttons-payment-gateway.php' );

    // Now that we have successfully included our class,
    // Lets add it too WooCommerce
    add_filter('woocommerce_payment_gateways', 'add_besteron_buttons_payment_gateway');

    function add_besteron_buttons_payment_gateway($methods) {
        $methods[] = 'WC_Besteron_Buttons_Payment_Gateway';
        return $methods;
    }

}

// Add custom action links
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'besteron_payment_buttons_action_links');

function besteron_payment_buttons_action_links($links) {
    $plugin_links = array(
        '<a href="' . admin_url('admin.php?page=wc-settings&tab=checkout') . '">' . __('Settings', 'besteron-buttons-payment-gateway') . '</a>',
    );

    // Merge our new link with the default ones
    return array_merge($plugin_links, $links);
}


//Use version 2.0 of the update checker.
require 'plugin-updates/plugin-update-checker.php';
$MyUpdateChecker = new PluginUpdateChecker_2_0 (
    'http://developer.besteron.sk/extensions/wordpress/woocomerce/metadata-buttons.json',
    __FILE__,
    "besteron_buttons"
);
