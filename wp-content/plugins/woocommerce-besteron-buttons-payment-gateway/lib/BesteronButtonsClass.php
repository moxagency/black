<?php

class BesteronButtons {

    //const urls
    const BESTERON_URL = 'http://payments.besteron.com/pay-request/';
    const BESTERON_URL_TEST = 'http://client.besteron.com/public/virtual-payment/request/';
    const BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL = 'http://client.besteron.com/public/payments-gateways-generator/';

    //config parametrs
    const PARAM_BESTERON_URL = 'PARAM_BESTERON_URL';
    const PARAM_BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL = 'BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL';
    const PARAM_DEMO_MODE = 'DEMO';
    const PARAM_CID = 'CID';
    const PARAM_KEY = 'KEY';
    const PARAM_VS = 'VS';
    const PARAM_TYPE = 'TYPE'; //or BIC/SWIFT
    const PARAM_CURR = 'CURR';
    const PARAM_RU = 'RU';
    const PARAM_AMNT = 'AMNT';
    const PARAM_RESULT = 'RESULT';
    const PARAM_SIGN= 'SIGN';
    const PARAM_EMAIL= 'EMAIL';

    //response
    const RESULT_OK = 'OK';
    const RESULT_FAIL = 'RESULT_FAIL';

    //currency
    const CURR_EURO = 978;

    public static $BANKS_BUTTONS = array(
        'GIBASKBX' => array(
            'code' => '0900',
            'name' => 'Slovenská sporiteľňa, a. s.',
            'image' => 'slsp',
            'label' => 'SporoPay',
        ),
        'TATRSKBX' => array(
            'code' => '1100',
            'name' => 'Tatra banka, a. s.',
            'image' => 'tatra',
            'label' => 'TatraPay',
        ),
        'CEKOSKBX' => array(
            'code' => '7500',
            'name' => 'Československá obchodná banka, a. s.',
            'image' => 'csob',
            'label' => 'Platobné tlačidlo',
        ),
        'SUBASKBX' => array(
            'code' => '0200',
            'name' => 'Všeobecná úverová banka, a. s.',
            'image' => 'vub',
            'label' => 'ePlatby VÚB',
        ),
        'UNCRSKBX' => array(
            'code' => '1111',
            'name' => 'UniCredit Bank',
            'image' => 'unicredit',
            'label' => 'UniPlatba',
        ),
        'POBNSKBA' => array(
            'code' => '6500',
            'name' => 'Poštová banka, a. s.',
            'image' => 'pb',
            'label' => 'Platba Online',
        ),
        'VIAMO' => array(
            'code' => '',
            'name' => 'VIAMO',
            'image' => 'viamo',
            'label' => 'VIAMO',
        )
    );

    /**
     * Unique Besteron customer ID. Required.
     * @var String
     */
    protected $CID;

    /**
     * Private key to crypt communication. Required.
     * @var String
     */
    protected $KEY;

    /**
     * Besteron url.
     * @var String
     */
    protected $BESTERON_URL;

    /**
     * Besteron payments gateways generator url.
     * @var String
     */
    protected $BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL;

    /**
     * Amount. Max 13+2 numbers. Decimal point ".". Required.
     * @var Float
     */
    protected $AMNT;

    /**
     * Variable symbol. Max 10 numbers. Required.
     * @var String
     */
    protected $VS;

    /**
     * Payment type TYPE
     * @var String
     */
    protected $TYPE;

    /**
     * Currency code
     * @var Int[3]
     */
    protected $CURR;

    public function __construct($cfg = array()) {

        if (!array_key_exists(self::PARAM_BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL, $cfg)) {
            $cfg[self::PARAM_BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL] = self::BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL;
        }
        if (!array_key_exists(self::PARAM_BESTERON_URL, $cfg) || array_key_exists(self::PARAM_DEMO_MODE, $cfg)) {
            $cfg[self::PARAM_BESTERON_URL] = $cfg[self::PARAM_DEMO_MODE]
                    ? self::BESTERON_URL_TEST : self::BESTERON_URL;
        }
        if (empty($cfg[self::PARAM_CID]) || empty($cfg[self::PARAM_KEY]) || empty($cfg[self::PARAM_BESTERON_URL]) || empty($cfg[self::PARAM_BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL])) {
            throw new Exception('Set variables in config file.' /* .  print_r($cfg, true) */);
        }

        $this->CID = $cfg[self::PARAM_CID];
        $this->BESTERON_URL = $cfg[self::PARAM_BESTERON_URL];
        $this->BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL = $cfg[self::PARAM_BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL];
        $this->KEY = $cfg[self::PARAM_KEY];
    }

    /**
     * Get amount.
     * @return string
     */
    public function getAmnt() {
        return $this->AMNT;
    }

    /**
     * Set amount.
     * @param string $amnt
     * @return Besteron
     */
    public function setAmnt($AMNT) {
        $this->AMNT = $this->sanitizeAmnt($AMNT);
        return $this;
    }

    /**
     * Get currency.
     * @return string
     */
    public function getCurr() {
        return $this->CURR;
    }

    /**
     * Set currency.
     * @param string $curr
     * @return Besteron
     */
    public function setCurr($CURR) {
        $this->CURR = $CURR;
        return $this;
    }

    /**
     * Get variable symbol.
     * @return string
     */
    public function getVs() {
        return $this->VS;
    }

    /**
     * Set variable symbol.
     * @param string $vs
     * @return Besteron
     */
    public function setVs($VS) {
        $this->VS = $VS;
        return $this;
    }

    /**
     * Return sign.
     * @return string
     */
    public function generateSign($str) {
        $key = $this->KEY;
        $sha1hash = sha1($str);
        $hexSha1_16 = substr($sha1hash, 0, 32);
        $method = 'aes-256-ecb';
        $key = pack('H*', $key);
        $ivSize = openssl_cipher_iv_length($method);
        $iv = openssl_random_pseudo_bytes($ivSize);
        $data = pack('H*', $hexSha1_16);
        $result = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);
        return strtoupper(bin2hex($result));
    }

    protected function sanitizeAmnt($AMNT) {

        return str_replace(',', '.', sprintf('%.2f', $AMNT));
    }

}

class BesteronButtonsRequest extends BesteronButtons {

    /**
     * Return url. Required
     * @var String
     */
    protected $RU;

    /**
     * Customer e-mail. REQUIRED.
     * @var String
     */
    protected $EMAIL;

    /**
     * @return Besteron
     */
    public function __construct($options = array(), $cfgPath = '') {

        parent::__construct($options, $cfgPath);

        if (!isset($options[self::PARAM_AMNT])) {
            throw new Exception('Amount is not defined.');
        }
        if (empty($options[self::PARAM_AMNT])) {
            throw new Exception('Amount is empty.');
        }
        if (!is_int($options[self::PARAM_AMNT]) && !is_float($options[self::PARAM_AMNT])) {
            throw new Exception('Amount is not a number.');
        }
        if (preg_match("/^\d+(\.\d{1,2})?$/", $options[self::PARAM_AMNT]) == 0) {
            throw new Exception('Amount format is not valid.');
        }
        if (!isset($options[self::PARAM_CURR])) {
            throw new Exception('Currency is not defined.');
        }
        if (empty($options[self::PARAM_CURR])) {
            throw new Exception('Currency is empty.');
        }
        if (!isset($options[self::PARAM_VS])) {
            throw new Exception('VariableSymbol is not defined.');
        }
        if (empty($options[self::PARAM_VS])) {
            throw new Exception('VariableSymbol is empty.');
        }
        if (!is_numeric($options[self::PARAM_VS])) {
            throw new Exception('VariableSymbol is not numeric.');
        }
        if (strlen($options[self::PARAM_VS]) > 10) {
            throw new Exception('VariableSymbol max length is 10.');
        }
        if (!isset($options[self::PARAM_RU])) {
            throw new Exception('ReturnUrl is not defined.');
        }
        if (empty($options[self::PARAM_RU])) {
            throw new Exception('ReturnUrl is empty.');
        }
        if (!isset($options[self::PARAM_CURR])) {
            throw new Exception('Currency is not defined.');
        }
        if (empty($options[self::PARAM_CURR])) {
            throw new Exception('Currency is empty.');
        }
        if(!isset($options[self::PARAM_EMAIL])) {
            throw new Exception('Customer e-mail is not defined.');
        }
        if(empty($options[self::PARAM_EMAIL])) {
            throw new Exception('Customer e-mail is empty.');
        }
        $this->setAmnt($options[self::PARAM_AMNT]);
        $this->setCurr($options[self::PARAM_CURR]);
        $this->setVs($options[self::PARAM_VS]);
        $this->setRu($options[self::PARAM_RU]);
        $this->setEmail($options[self::PARAM_EMAIL]);

        return $this;
    }

    /**
     * Return return url.
     * @return string
     */
    public function getRu() {

        return $this->RU;
    }

    /**
     * Set return url.
     * @param string $returnUrl
     * @return Besteron
     */
    public function setRu($RU) {

        $this->RU = $RU;
        return $this;
    }

    /**
     * Get customer email.
     * @return String
     */
    public function getEmail()
    {
        return $this->EMAIL;
    }

    /**
     * Set customer email
     * @param String $EMAIL
     * return Besteron
     */
    public function setEmail($EMAIL)
    {
        $this->EMAIL = $EMAIL;
        return $this;
    }

    public function getUrl($TYPE, $USED_BANKS) {
        //valid bic/swift
        if (!array_key_exists($TYPE, $USED_BANKS)){
            throw new Exception('Invalid parameter type.');
        }
        $this->TYPE = $TYPE;

        $params = array(
            'cid=' . $this->CID,
            'type=' . $TYPE,
            'amnt=' . $this->AMNT,
            'curr=' . $this->CURR,
            'vs=' . $this->VS,
            'ru=' . urlencode($this->RU),
            'email=' . urlencode($this->EMAIL),
            'sign=' . $this->generateSign($this->getString())
            //odkomentovat pre prostredie platobnej brany v EN jazyku
            //,'lang=EN'
        );

        $url = $this->BESTERON_URL . '?' . implode('&', $params);
        return $url;
    }

    public function getGeneratorUrl() {

        $params = array(
            'cid=' . $this->CID,
            'amnt=' . $this->AMNT,
            'curr=' . $this->CURR,
            'vs=' . $this->VS,
            'ru=' . urlencode($this->RU),
            'email=' . urlencode($this->EMAIL),
            'sign=' . $this->generateSign($this->getString(false))
        );

        $url = $this->BESTERON_PAYMENTS_GATEWAYS_GENERATOR_URL . '?' . implode('&', $params);
        return $url;
    }

    /**
     * Return string for hash to sign.
     * @var String
     */
    public function getString($type = true) {

        $string = $this->CID . ($type ? $this->TYPE : '') . $this->AMNT . $this->CURR . $this->VS . $this->RU;

        return $string;
    }

}

class BesteronButtonsResponse extends BesteronButtons {

    /**
     * Status. Required
     * @var String
     */
    protected $RESULT;

    /**
     * Sign from besteron.
     * @var String
     */
    protected $SIGN;

    /**
     * @return Besteron
     */
    public function __construct($options = array(), $cfgPath = '') {

        parent::__construct($options, $cfgPath);

        if (!isset($options[self::PARAM_TYPE])) {
            throw new Exception('TYPE is not defined.');
        }
        if (empty($options[self::PARAM_TYPE])) {
            throw new Exception('TYPE code is empty.');
        }
        if (!isset($options[self::PARAM_AMNT])) {
            throw new Exception('Amount is not defined.');
        }
        if (empty($options[self::PARAM_AMNT])) {
            throw new Exception('Amount is empty.');
        }
        if (!isset($options[self::PARAM_CURR])) {
            throw new Exception('Currency is not defined.');
        }
        if (empty($options[self::PARAM_CURR])) {
            throw new Exception('Currency is empty.');
        }
        if (!isset($options[self::PARAM_VS])) {
            throw new Exception('VariableSymbol is not defined.');
        }
        if (!isset($options[self::PARAM_VS])) {
            throw new Exception('VariableSymbol is empty.');
        }
        if (!is_numeric($options[self::PARAM_VS])) {
            throw new Exception('VariableSymbol is not numeric.');
        }
        if (strlen($options[self::PARAM_VS]) > 10) {
            throw new Exception('VariableSymbol max length is 10.');
        }
        if (!isset($options[self::PARAM_RESULT])) {
            throw new Exception('Result is not defined.');
        }
        if (empty($options[self::PARAM_RESULT])) {
            throw new Exception('Result is empty.');
        }
        if (!isset($options[self::PARAM_SIGN])) {
            throw new Exception('Sign is not defined.');
        }
        if (empty($options[self::PARAM_SIGN])) {
            throw new Exception('Sign is empty.');
        }

        $this->TYPE = $options[self::PARAM_TYPE];
        $this->AMNT = $options[self::PARAM_AMNT];
        $this->CURR = $options[self::PARAM_CURR];
        $this->VS = $options[self::PARAM_VS];
        $this->RESULT = $options[self::PARAM_RESULT];
        $this->SIGN = $options[self::PARAM_SIGN];

        return $this;
    }

    /**
     * Get result
     * @return string
     */
    public function getResult() {
        return $this->RESULT;
    }

    /**
     * Set result.
     * @param string $result
     * @return string
     */
    public function setResult($RESULT) {
        $this->RESULT = $RESULT;
        return $this;
    }

    /**
     * Return sign.
     * @return string
     */
    public function getSign() {

        return $this->SIGN;
    }

    /**
     * Set sign.
     * @param string $sign
     * @return Besteron
     */
    public function setSign($SIGN) {

        $this->SIGN = $SIGN;
        return $this;
    }

    /**
     * Generate string for generetaSign();
     * @return string
     */
    public function getString() {

        $string = $this->CID . $this->TYPE . $this->AMNT . $this->CURR . $this->VS . $this->RESULT;
        return $string;
    }

    public function isValid() {

        return $this->generateSign($this->getString()) == $this->SIGN;
    }

}
