��          �            x  1   y  8   �     �  	         
          7     S  6   r     �     �     �     �  =   �  =   ;  �  y  '   .      V     w     �     �     �     �     �  3        A  !   _  
   �     �     �     �                                      
          	                               An error occurred. The payment has not validated. Besteron: Bank is not selected. Select please your bank. Besteron: Unknown BIC/SWIFT Demo mode Enable / Disable Enable this payment gateway Mediated by payment gateway Online Buttons Payment Gateway Online Buttons Payment Gateway Plug-in for WooCommerce Payment transaction failed Plugin is in demo mode Settings Unknown server response: Your Merchant CID from your BESTERON account Integration page Your Merchant Key from your BESTERON account Integration page Project-Id-Version: Besteron Payment Gateway v1.0.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-17 16:27+0200
PO-Revision-Date: 2016-08-17 16:29+0200
Last-Translator: admin <ondrej@kyzek.sk>
Language-Team: 
Language: sk_SK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Generator: Poedit 1.8.8
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: languages
X-Poedit-SearchPath-1: .
 Došlo k chybe. Platba nebola overená. Besteron: Nie je vybraná banka. Besteron: Neznámy BIC/SWIFT Demo mod Povoliť / Zakázať Povoliť túto platobnú bránu Sprostredkuje platobná brána Online Platobné Tlačidlá Online Platobné Tlačidlá Plug-in pre WooCommerce Platobná transakcia zlyhala. Plugin je v testovacej prevádzke Nastavenia Neznáma odpoveď servra Zadajte Váš besteron CID  Zadajte Váš besteron KEY 