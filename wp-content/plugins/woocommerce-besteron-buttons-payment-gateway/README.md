Woocommerce payment plugin for Besteron gateway (iba platobné tlačidlá)
===============================================

Instal
------

Configure
---------

Deploy:
-------
1. json configurak na http://developer.besteron.sk/extensions/wordpress/woocomerce/metadata-buttons.json 

```javascript
//metadata.json
{
    "name" : "Besteron Plugin",
    "slug" : "besteron",
    "download_url" : "http://developer.besteron.sk/extensions/wordpress/woocomerce/besteron-buttons-woocommerce-plugin-1.0.8.zip",
    "version" : "1.0.8",
    "author" : "Besteron",
    "sections" : {
        "description" : "Activation of payments gateways."
    }
}
```

2. Zbalit adresar do zip podla prislusneho url, dat na server, vyhodit Readme.md