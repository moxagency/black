<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header <?php if (get_queried_object_id() == 16) echo 'category--accesories'; elseif (get_queried_object_id() == 17) echo 'category--coffee'?>">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

    <?php
        // CATEGORIES

        $taxonomy     = 'product_cat';
        $orderby      = 'name';
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title        = '';
        $empty        = 0;

        $args = array(
            'taxonomy'     => $taxonomy,
            'orderby'      => $orderby,
            'show_count'   => $show_count,
            'pad_counts'   => $pad_counts,
            'hierarchical' => $hierarchical,
            'title_li'     => $title,
            'hide_empty'   => $empty
        );
        $all_categories = get_categories( $args );
        echo '<div class="product-categories">';
        echo '<div class="product-categories-wrapper">';
        foreach ($all_categories as $cat) {
            if($cat->category_parent == 0) {
                $category_id = $cat->term_id;
                echo '<div class="product-categories__item">';
                echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';
                echo '</div>';
            }
        }
        echo '</div>';

        echo '<a href="#" class="advanced-filter" id="advanced-filter">Rozšírený filter</a>';
        echo '</div>';
    ?>

    <div class="archive__product-filter">
        <div class="archive__product-filter__item archive__product-filter__item--range">
            <h5 class="archive__product-filter__heading">Cena</h5>

            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Price range") ) : ?>
            <?php endif;?>
        </div>

        <div class="archive__product-filter__item archive__product-filter__item--dropdown">
            <h5 class="archive__product-filter__heading">Brand</h5>

            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Brand Filter") ) : ?>
            <?php endif;?>
        </div>

        <?php
        // show only if coffee
        $category = get_queried_object_id();

        if ($category != 16) {
            echo '<div class="archive__product-filter__item archive__product-filter__item--list">';
            echo '<h5 class="archive__product-filter__heading">Praženie</h5>';

            if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Roast Filter") ) :
            endif;

            echo '</div>';
        }

        ?>

        <?php if ( in_category( 4 ) ) : ?>
            <div class="event-post-icons">
                <ul>
                    <li class="single-post-date"><?php the_field('event_date'); ?></li>
                    <li class="single-post-time"><?php the_field('event_time'); ?></li>
                    <li class="single-post-price"><?php the_field('booking_fee'); ?></li>
                    <li class="single-post-palce"><?php the_field('event_location'); ?></li>
                </ul>
            </div><!-- /event-post-icons -->
        <?php endif; ?>
    </div>
    <?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php

if ( have_posts() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
