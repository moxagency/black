(function($) {

    $('.archive__product-filter').hide();
    $('#advanced-filter').on('click', function () {

        $('.archive__product-filter').slideToggle(200);

        return false;
    });

    // side cart price
    $('.xoo-wsc-price').empty();

    // remove image zoom

    $('.woocommerce-product-gallery__image a').on('click', function () {
        return false;
    });

    // contact info

    $("#web-conditions-btn").on('click', function() {
        $("#web-conditions").fadeIn();
        $("#gdpr").hide();

        return false;
    });
    $("#gdpr-btn").on('click', function() {
        $("#gdpr").fadeIn();
        $("#web-conditions").hide();

        return false;
    });

    // shippping checkout
    $('.shipping td > *').unwrap();

    // animations

    // sroll magic
    var controller = new ScrollMagic.Controller();

    $('.block__heading').each(function () {
        var fadeInTween = TweenMax.from(this, .6, {autoAlpha: 0});

        var loadScene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: .85
        })
            .setTween(fadeInTween)
            .addTo(controller);
    });

    // Homepage intro text fade in
    var ourStoryTween = TweenMax.from($('.our-story p'), .6, {autoAlpha: 0});

    var ourStoryFadeIn = new ScrollMagic.Scene({
        triggerElement: '.our-story p',
        triggerHook: .9
    })
        .setTween(ourStoryTween)
        .addTo(controller);


    // Homepage coffees timeline
    var coffeesTl = new TimelineLite();

    coffeesTl.staggerFrom($('.coffee'), 1.2, {autoAlpha:0}, 0.2);

    var CoffeeFadeIn = new ScrollMagic.Scene({
        triggerElement: '.coffees .row',
        triggerHook: .8
    })
        .setTween(coffeesTl)
        .addTo(controller);

    // Homepage featured product timeline
    var featuredProductsTl = new TimelineLite();

    featuredProductsTl.staggerFrom($('.featured-product'), 1.2, {autoAlpha:0}, 0.2);

    var productFadeIn = new ScrollMagic.Scene({
        triggerElement: '.featured-products .products',
        triggerHook: .8
    })
        .setTween(featuredProductsTl)
        .addTo(controller);

    // Homepage Places fade in
    var placesTween = TweenMax.from($('.places__item'), .6, {autoAlpha: 0});

    var placesFadeIn = new ScrollMagic.Scene({
        triggerElement: '.places .row',
        triggerHook: .8
    })
        .setTween(placesTween)
        .addTo(controller);

    // Homepage instagram fade in
    var instagramTween = TweenMax.from($('.instagram__logo'), .6, {autoAlpha: 0});

    var instagramFadeIn = new ScrollMagic.Scene({
        triggerElement: '.instagram__logo',
        triggerHook: .8
    })
        .setTween(instagramTween)
        .addTo(controller);

    var instagramContentTween = TweenMax.from($('#sb_instagram'), .6, {autoAlpha: 0});

    var instagramContentFadeIn = new ScrollMagic.Scene({
        triggerElement: '#sb_instagram',
        triggerHook: .8
    })
        .setTween(instagramContentTween)
        .addTo(controller);

    // only for desktop devices
    if ($( window ).width() >= 1024) {

        // setting pin for sidebar
        var pinDuration = $('.blog-sidebar-pin').height();

        var pinScene = new ScrollMagic.Scene({
            triggerElement: '.blog-sidebar',
            triggerHook: 0,
            offset: -85,
            duration: pinDuration
        })
            .setPin('.blog-sidebar', {pushFollowers: false})
            .addTo(controller);
    }

})(jQuery);