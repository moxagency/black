<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <main class="site-main" id="main">
                <div class="blog-sidebar-pin">

                    <div class="blog__latest-post">
                        <?php $the_query = new WP_Query( 'posts_per_page=1' ); ?>

                        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                            <?php get_template_part( 'loop-templates/content', get_post_format() ); ?>

                            <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>

                    <div class="blog__authors">
                        <ul>
                            <?php
                            global $wpdb;
                            $authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users ORDER BY display_name");

                            foreach($authors as $author) {
                                echo '<li class="blog__author" style="background-image: url('. $get_author_gravatar = get_avatar_url($author->ID, array('size' => 450)) .')">';
                                echo '<div class="blog__author-overlay"></div>';
                                echo "<a class='blog__author-avatar' href=\"".get_bloginfo('url')."/?author=";
                                echo $author->ID;
                                echo "\">";
                                echo get_avatar($author->ID);
                                echo "</a>";
                                echo "<a class='blog__author-name' href=\"".get_bloginfo('url')."/?author=";
                                echo $author->ID;
                                echo "\">";
                                the_author_meta('display_name', $author->ID);
                                echo "</a>";
                                echo "</li>";
                            }
                            ?>
                        </ul>
                    </div>

                    <?php if ( have_posts() ) : ?>

                        <?php /* Start the Loop */ ?>

                        <?php query_posts('offset=1'); ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php

                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'loop-templates/content', get_post_format() );
                            ?>

                        <?php endwhile; ?>

                    <?php else : ?>

                        <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                    <?php endif; ?>
                </div>
            </main>
        </div>

        <div class="col-lg-1"></div>

        <div class="col-lg-3">
            <div class="blog-sidebar">
                <h3 class="down blog-sidebar__heading">Najčítanejšie</h3>

                <?php
                $popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );

                echo '<ul class="list-unstyled blog-sidebar__list">';

                while ( $popularpost->have_posts() ) : $popularpost->the_post();

                    echo '<li><a href="'. get_permalink() .'">';
                    the_title();
                    echo '</a></li>';

                endwhile;

                echo '</ul>';
                ?>

                <h3 class="down blog-sidebar__heading">Kategórie</h3>

                <?php
                $categories =  get_categories();
                echo '<ul class="list-unstyled blog-sidebar__list">';
                foreach  ($categories as $category) {
                    echo '<li><a href="' . get_category_link($category->term_id) . '">'. $category->cat_name .'</a></li>';
                }
                echo '</ul>';
                ?>
            </div>
        </div>

        <!-- The pagination component -->
        <?php understrap_pagination(); ?>
    </div>
</div>

<?php get_footer(); ?>
