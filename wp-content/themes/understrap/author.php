<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="container">
    <?php
    $curauth = ( isset( $_GET['author_name'] ) ) ? get_user_by( 'slug',
        $author_name ) : get_userdata( intval( $author ) );
    ?>

    <h1 class="entry-title"><?php echo esc_html( $curauth->display_name ); ?></h1>

    <p class="author__description">
        <?php echo esc_html( $curauth->user_description ); ?>
    </p>

    <div class="author__posts">
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php

                /*
                 * Include the Post-Format-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
                get_template_part( 'loop-templates/content', get_post_format() );
                ?>
            <?php endwhile; ?>

        <?php else : ?>

            <?php get_template_part( 'loop-templates/content', 'none' ); ?>

        <?php endif; ?>
    </div>

    <?php understrap_pagination(); ?>
</div>

<?php get_footer(); ?>
