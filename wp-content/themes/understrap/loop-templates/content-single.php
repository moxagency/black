<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

    <?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <header class="entry-header">

                    <div class="entry-meta">

                        <strong><?php the_category( ' ' ); ?></strong>
                        <span class="entry-meta__divider">/</span>
                        <?php
                            echo "<a href=\"".get_bloginfo('url')."/?author=";
                            echo get_the_author_meta( 'ID' );
                            echo "\">";
                            echo get_the_author_meta( 'first_name' );
                            echo "</a>";
                        ?>
                        <span class="entry-meta__divider">/</span>
                        <?php echo get_the_date(); ?>,
                        <?php the_time( 'H:i' ); ?>


                    </div><!-- .entry-meta -->

                    <?php the_title( '<h1 class="single-post__title">', '</h1>' ); ?>

                    <?php
                        echo "<a class='single-post__author-avatar' href=\"".get_bloginfo('url')."/?author=";
                        echo get_the_author_meta( 'ID' );
                        echo "\">";
                        echo get_avatar( get_the_author_meta( 'ID' ) , 75 );
                        echo "</a>";
                    ?>


                </header><!-- .entry-header -->

                <div class="entry-content">
                    <div class="blog-sidebar-pin">
                        <?php the_content(); ?>

                        <?php
                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
                            'after'  => '</div>',
                        ) );
                        ?>
                    </div>
                </div><!-- .entry-content -->
            </div>

            <div class="col-lg-3">
                <div class="blog-sidebar">
                    <div class="blog-sidebar__author">
                        <?php
                            echo "<a class='blog-sidebar__author-avatar' href=\"".get_bloginfo('url')."/?author=";
                            echo get_the_author_meta( 'ID' );
                            echo "\">";
                            echo get_avatar( get_the_author_meta( 'ID' ) , 75 );
                            echo "<h5 class='blog-sidebar__author-name'>";
                            echo get_the_author_meta( 'first_name' );
                            echo "</h5>";
                            echo "</a>";
                        ?>
                    </div>

                    <h3 class="down blog-sidebar__heading">Najčítanejšie</h3>

                    <?php
                    $popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );

                    echo '<ul class="list-unstyled blog-sidebar__list">';

                    while ( $popularpost->have_posts() ) : $popularpost->the_post();

                        echo '<li><a href="'. get_permalink() .'">';
                        the_title();
                        echo '</a></li>';

                    endwhile;

                    echo '</ul>';
                    ?>

                    <h3 class="down blog-sidebar__heading">Kategórie</h3>

                    <?php
                    $categories =  get_categories();
                    echo '<ul class="list-unstyled blog-sidebar__list">';
                    foreach  ($categories as $category) {
                        echo '<li><a href="' . get_category_link($category->term_id) . '">'. $category->cat_name .'</a></li>';
                    }
                    echo '</ul>';
                    ?>
                </div>
            </div>
        </div>
    </div>
</article>