<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
    <div class="post__overlay"></div>

    <div class="entry-content">
        <?php the_title( sprintf( '<h2 class="post__title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
            '</a></h2>' ); ?>

        <?php echo get_avatar( get_the_author_meta( 'ID' ) , 50 ); ?>
    </div>
</article><!-- #post-## -->
