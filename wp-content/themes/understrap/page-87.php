<?php
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<main class="contact">
    <div class="container">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

        <div class="row">
            <div class="col-lg-6 contact-form">
                <div class="down-wrapper">
                    <h3 class="down down--big contact-form__heading">Kontaktný formulár</h3>
                </div>

                <?php echo do_shortcode('[contact-form-7 id="91" title="kontakt"]') ?>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <div class="contact-info">
                    <p class="contact-info__heading">Black.xyz s.r.o.</p>

                    <p>
                        Gorkého 15, <br>
                        Bratislava - mestská časť staré mesto <br>
                        811 01
                    </p>

                    <p>
                        IČO: 51 057 093 <br>
                        DIČ: 2120576568
                    </p>
                </div>

                <div class="contact-text">
                    <a href="#" class="down contact-text__btn" id="web-conditions-btn">Všeobecné podmienky</a>
<!--                    <a href="#" class="down contact-text__btn" id="gdpr-btn">GDPR</a>-->

                    <p id="web-conditions">
                        <strong>PREDÁVAJÚCI</strong> <br>

                        black.xyz s.r.o. <br>

                        Gorkého 15, <br>
                        Bratislava - Mestská časť Staré mesto <br>
                        811 01 <br> <br>

                        IČO: 51 057 093 <br>
                        DIČ: 2120576568 <br><br>

                       <strong>PREVÁDZKA A OSOBNÝ ODBER</strong> <br>

                        Gorkého 15, <br>
                        Bratislava - Mestská časť Staré mesto <br>
                        811 01 <br> <br>

                        Kupujúci (zákazník) potvrdením objednávky potvrdzuje, že akceptuje obchodné podmienky pre dodávku tovaru vyhlásené predávajúcim.
                        <br><br>

                        <a href="../black-vseobecne-podmienky.pdf" target="_"><strong>ČÍTAŤ VIAC</strong></a>
                    </p>

                    <p id="gdpr">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A autem consectetur cum delectus dolorem doloribus fuga fugit, iusto laudantium, nemo nihil non nostrum perspiciatis quam reprehenderit saepe velit veritatis voluptatibus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore facere quasi rem repellat. Accusamus architecto consequatur cum doloremque earum ipsa ipsam ipsum labore magni molestias, nulla omnis possimus quaerat quisquam.
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>

