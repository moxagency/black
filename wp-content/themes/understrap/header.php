<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <title>
        <?php
            if (is_archive()) {
               echo ' Shop - ';
               echo bloginfo('name');
            }

            elseif (is_front_page()) {
                echo bloginfo('name');
            }

            else {
                wp_title('');
            }
        ?>
    </title>

    <!-- fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;subset=latin-ext" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

<header class="site-header">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="<?php bloginfo( 'url' ) ?>"><img src="<?php imagesUrl('logo-black.svg') ?>" alt="Logo" class="site-header__logo"></a>
            </div>
            <div class="col-md-8">
                <?php wp_nav_menu(
                    array(
                        'theme_location'  => 'primary',
                        'container_class' => 'site-navigation',
                        'menu_class'      => 'site-navigation__list list-inline',
                        'menu_id'         => 'main-menu',
                        'walker'          => new understrap_WP_Bootstrap_Navwalker(),
                    )
                ); ?>
            </div>
        </div>
    </div>
</header>
