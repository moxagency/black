<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

/**
 * Initialize theme default settings
 */
require get_template_directory() . '/inc/theme-settings.php';

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Register widget area.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom pagination for this theme.
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Comments file.
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Load WooCommerce functions.
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load Editor functions.
 */
require get_template_directory() . '/inc/editor.php';

// Echo full image path from given image name
function imagesUrl($file = null){
    echo get_stylesheet_directory_uri() . '/img/' . $file;
}

// GSAP script, Scroll magic
function enqueue_TweenMax() {
    if ( ! is_admin() ) {
        $scriptsrc = get_template_directory_uri() . '/js/TweenMax.min.js';
        wp_register_script( 'TweenMax', $scriptsrc );
        wp_enqueue_script( 'TweenMax' );
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_TweenMax' );

function enqueue_ScrollMagic() {
    if ( ! is_admin() ) {
        $scriptsrc = get_template_directory_uri() . '/js/ScrollMagic.min.js';
        wp_register_script( 'ScrollMagic', $scriptsrc );
        wp_enqueue_script( 'ScrollMagic' );
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_ScrollMagic' );

function enqueue_Animation() {
    if ( ! is_admin() ) {
        $scriptsrc = get_template_directory_uri() . '/js/animation.gsap.min.js';
        wp_register_script( 'Animation', $scriptsrc );
        wp_enqueue_script( 'Animation' );
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_Animation' );

function enqueue_Timeline() {
    if ( ! is_admin() ) {
        $scriptsrc = get_template_directory_uri() . '/js/TimelineLite.min.js';
        wp_register_script( 'Timeline', $scriptsrc );
        wp_enqueue_script( 'Timeline' );
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_Timeline' );


// SHOP

remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );


/**
 * Display product attribute archive links
 */

function nt_product_attributes() {
    global $product;
    if ( $product->has_attributes() ) {

        $attributes = array (
            'brand'              => $product->get_attribute( 'pa_brand' ),
            'additionals'            => $product->get_attribute( 'pa_additionals' ),
        );
    return $attributes;
    }
}

function display_attributes() {
    $attributes = nt_product_attributes();
    echo '<p class="brand">' . $attributes['brand'] . '</p>';
    echo '<p class="additionals">' . $attributes['additionals'] . '</p>';
}

add_action( 'woocommerce_shop_loop_item_title', 'display_attributes' );

/**
 * Product filter
 */

add_action( 'widgets_init', 'my_awesome_sidebar' );
function my_awesome_sidebar() {

    $my_sidebars = array(
        array(
            'name'          => 'Brand Filter',
            'id'            => 'brand-filter',
            'description'   => 'Widgets shown in the flyout of the header',
        ),
        array(
            'name'          => 'Roast Filter',
            'id'            => 'roast-filter',
            'description'   => 'Widgets shown in the flyout of the header',
        ),
        array(
            'name'          => 'Price range',
            'id'            => 'price-range',
            'description'   => 'Widgets shown in the flyout of the header',
        ),
    );

    $defaults = array(
        'name'          => 'Product Filter',
        'id'            => 'product-filter',
        'class'         => 'product-filter',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    );

    foreach( $my_sidebars as $sidebar ) {
        $args = wp_parse_args( $sidebar, $defaults );
        register_sidebar( $args );
    }

}


/**
 * Display least price on variable product
 */

add_filter('woocommerce_variable_sale_price_html', 'shop_variable_product_price', 10, 2);
add_filter('woocommerce_variable_price_html','shop_variable_product_price', 10, 2 );
function shop_variable_product_price( $price, $product ){
    $variation_min_reg_price = $product->get_variation_regular_price('min', true);
    $variation_min_sale_price = $product->get_variation_sale_price('min', true);
    if ( $product->is_on_sale() && !empty($variation_min_sale_price)){
        if ( !empty($variation_min_sale_price) )
            $price = '<del class="strike">' .  wc_price($variation_min_reg_price) . '</del>
        <ins class="highlight">' .  wc_price($variation_min_sale_price) . '</ins>';
    } else {
        if(!empty($variation_min_reg_price))
            $price = '<ins class="highlight">'.wc_price( $variation_min_reg_price ).'</ins>';
        else
            $price = '<ins class="highlight">'.wc_price( $product->regular_price ).'</ins>';
    }
    return $price;
}

/**
 * Remove heading and path
 */

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

/**
 * Remove add to cart button
 */

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );

// SINGLE PRODUCT

/**
 * Removing stuff
 */

function remove_image_zoom_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'wp', 'remove_image_zoom_support', 100 );

//remove_action( 'woocommerce_before_single_product', 'wc_print_notices', 10 );

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_theme_support( 'wc-product-gallery-zoom' );


/**
 * Variations - single variation PRICES
 */

remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
add_action( 'woocommerce_before_variations_form', 'woocommerce_single_variation', 5 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_template_single_price', 11 );

/**
 * Display reviews
 */

add_action( 'woocommerce_after_single_product', '_show_reviews', 15 );
function _show_reviews() {
    comments_template();
}

/**
 * Reviews
 */

remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar', 10 );

/**
 * Add roastery to a product
 */

function display_brand() {
    $attributes = nt_product_attributes();
    echo '<p class="brand">' . $attributes['brand'] . '</p>';

}

add_action( 'woocommerce_after_title', 'display_brand' );

// CHECKOUT

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

/**
 * Placeholders
 */

add_filter('woocommerce_default_address_fields', 'override_address_fields');
function override_address_fields( $address_fields ) {
    $address_fields['address_1']['placeholder'] = '';
    $address_fields['address_2']['placeholder'] = 's';
    return $address_fields;
}

// WooCommerce Checkout Fields Hook
add_filter( 'woocommerce_checkout_fields' , 'custom_wc_checkout_fields' );

// Change order comments placeholder and label, and set billing phone number to not required.
function custom_wc_checkout_fields( $fields ) {
    $fields['order']['order_comments']['placeholder'] = '';
    $fields['billing']['billing_phone']['required'] = false;
    return $fields;
}

/**
 * HTML Tags
 */

if(!function_exists('thmlv_add_allowed_tags')) {
    function thmlv_add_allowed_tags($tags) {
        $tags['time'] = array(
            'datetime' => true,
        );
        $tags['span'] = array(
            'class' => true,
            'id' => true
        );
        $tags['a'] = array(
            'class' => true,
            'id' => true,
            'href' => true
        );
        return $tags;
    }
    add_filter('wp_kses_allowed_html', 'thmlv_add_allowed_tags');
}

/**
 * Order button text
 */

add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' );

function woo_custom_order_button_text() {
    return __( 'Objednať', 'woocommerce' );
}

/**
 * Shipping
 */

add_filter( 'woocommerce_cart_no_shipping_available_html', 'change_no_shipping_text' ); // Alters message on Cart page
add_filter( 'woocommerce_no_shipping_available_html', 'change_no_shipping_text' ); // Alters message on Checkout page
function change_no_shipping_text() {

    return "Zadajte adresu";
}

/**
 * Set post views count using post meta
 */

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

/**
 * remove website from form
 */

add_filter('comment_form_default_fields', 'website_remove');
function website_remove($fields)
{
    if(isset($fields['url']))
        unset($fields['url']);
    return $fields;
}

/**
 * PRODUCT TAGS
 */

// Adding a custom Meta container to admin products pages
add_action( 'add_meta_boxes', 'create_custom_meta_box' );
if ( ! function_exists( 'create_custom_meta_box' ) )
{
    function create_custom_meta_box()
    {
        add_meta_box(
            'custom_product_meta_box',
            __( 'Tagy ku produktu', 'woocommerce' ),
            'add_custom_product_content_meta_box',
            'product',
            'normal',
            'default'
        );
    }
}

//  Custom metabox content in admin product pages
if ( ! function_exists( 'add_custom_product_content_meta_box' ) ){
    function add_custom_product_content_meta_box( $post ){
        $text_area = get_post_meta($post->ID, '_custom_text', true) ? get_post_meta($post->ID, '_custom_text', true) : '';
        $args['textarea_rows'] = 6;

        wp_editor( $text_area, 'custom_text', $args );

        echo '<input type="hidden" name="custom_text_field_nonce" value="' . wp_create_nonce() . '">';
    }
}

//Save the data of the Meta field
add_action( 'save_post_product', 'save_custom_product_content_meta_box', 20, 3 );
if ( ! function_exists( 'save_custom_product_content_meta_box' ) ){
    function save_custom_product_content_meta_box( $post_id, $post, $update  ) {

        // Check if our nonce is set.
        if ( ! isset( $_POST[ 'custom_text_field_nonce' ] ) )
            return $post_id;

        //Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $_POST[ 'custom_text_field_nonce' ] ) )
            return $post_id;

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;

        // Check the user's permissions.
        if ( ! current_user_can( 'edit_product', $post_id ) )
            return $post_id;

        // Sanitize user input and update the meta field in the database.
        if ( isset( $_POST[ 'custom_text' ] ) )
            update_post_meta( $post_id, $prefix.'_custom_text', wp_kses_post($_POST[ 'custom_text' ]) );
    }
}

## ---- 2. Frontend ---- ##

// Add custom text under single product meta
add_action( 'woocommerce_single_product_tags', 'add_custom_product_text', 70 );
function add_custom_product_text() {
    global $product;

    $custom_text = get_post_meta( $product->get_id(), '_custom_text', true );

    if( empty($custom_text) ) return;

    // Updated to apply the_content filter to WYSIWYG content
    echo apply_filters( 'the_content', $custom_text );

    echo '</div>';
}

/**
 * required phone number
 */