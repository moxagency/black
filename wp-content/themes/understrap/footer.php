<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<footer class="site-footer">
    <div class="container">
        <ul class="site-footer__contact list-inline">
            <li class="list-inline-item"><a href="mailto:ahoj@black.xyz">Ahoj@black.xyz</a></li>
            <li class="list-inline-item"><a href="http://www.black.xyz">black.xyz</a></li>
            <li class="list-inline-item"><a href="https://www.instagram.com/explore/tags/blackxyz/">#blackxyz</a></li>
        </ul>

        <ul class="site-footer__social list-inline">
            <li class="list-inline-item"><a href="https://www.facebook.com/black.svk/"><i class="fab fa-facebook-square"></i></a></li>
            <li class="list-inline-item"><a href="https://www.instagram.com/black.svk/"><i class="fab fa-instagram"></i></a></li>
        </ul>
    </div>
</footer>

<aside class="made-by">
    <div class="container">
        <a href="http://papayapos.com" target="_blank"><img src="<?php imagesUrl('papaya.png') ?>" alt="Papaya" class="made-by__papaya"></a>
        <a href="https://www.facebook.com/Lookasy/" target="_blank"><h1 class="made-by__lookasy">LOOKASY</h1></a>
        <a href="http://junkngold.com" target="_blank"><img src="<?php imagesUrl('junkngold.svg') ?>" alt="Junk'n Gold" class="made-by__junkngold"></a>
    </div>
</aside>

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

