<?php
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<main>
    <div class="slider">
        <?php
        echo do_shortcode('[smartslider3 slider=2]');
        ?>
    </div>

    <section class="our-story block">
        <div class="container">
            <h1 class="block__heading">Náš príbeh</h1>

            <p>
                <?php
                    $id = 2;
                    $post = get_post($id);
                    $content = apply_filters('the_content', $post->post_content);
                    echo $content;
                ?>
            </p>
        </div>
    </section>

    <section class="coffees block">
        <div class="container">
            <h1 class="block__heading">Momentálne pripravujeme</h1>

            <div class="row">
                <div class="col-lg-4">
                    <div class="coffee">
                        <img src="<?php imagesUrl('cup-icon.svg') ?>" alt="Icon" class="coffee__icon coffee__icon--cup">
                        <h4 class="coffee__type">Espresso 1</h4>
                        <h4 class="coffee__name"><?php do_action( 'espresso1' ); ?></h4>
                        <div class="coffee__roastery"><?php do_action( 'espresso1_roastery' ); ?></div>
                        <div class="coffee__taste-profile"><?php do_action( 'espresso1_tastingprofile' ); ?></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="coffee">
                        <img src="<?php imagesUrl('cup-icon.svg') ?>" alt="Icon" class="coffee__icon coffee__icon--cup">
                        <h4 class="coffee__type">Espresso 2</h4>
                        <h4 class="coffee__name"><?php do_action( 'espresso2' ); ?></h4>
                        <div class="coffee__roastery"><?php do_action( 'espresso2_roastery' ); ?></div>
                        <div class="coffee__taste-profile"><?php do_action( 'espresso2_tastingprofile' ); ?></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="coffee">
                        <img src="<?php imagesUrl('filter-icon.svg') ?>" alt="Icon" class="coffee__icon">
                        <h4 class="coffee__type">Filter</h4>
                        <h4 class="coffee__name"><?php do_action( 'filter' ); ?></h4>
                        <div class="coffee__roastery"><?php do_action( 'filter_roastery' ); ?></div>
                        <div class="coffee__taste-profile"><?php do_action( 'filter_tastingprofile' ); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="images block">
        <?php $the_query = new WP_Query( 'posts_per_page=2' ); ?>

        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

            <?php get_template_part( 'loop-templates/content', get_post_format() ); ?>

            <?php
        endwhile;
        wp_reset_postdata();
        ?>
    </section>

    <section class="featured-products block">
        <div class="container">
            <h1 class="block__heading">Produkty</h1>


            <?php
            $args = array(
                'posts_per_page' => 4,
                'post_type'      => 'product',
                'post_status'    => 'publish',
                'tax_query'      => array(
                    array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                        'operator' => 'IN',
                    ),
                )
            );
            $featured_product = new WP_Query( $args );
            if ( $featured_product->have_posts() ) :
                echo '<div class="woocommerce"><ul class="products">';
                while ( $featured_product->have_posts() ) : $featured_product->the_post();
                    $post_thumbnail_id     = get_post_thumbnail_id();
                    $post_price            = number_format((float)get_post_meta( get_the_ID(), '_price', true), 2, ',', ',');
                    $additionals           = get_the_terms( $post, 'additionals' );
                    $product_thumbnail     = wp_get_attachment_image_src($post_thumbnail_id, $size = 'shop-feature');
                    $product_thumbnail_alt = get_post_meta( $post_thumbnail_id, '_wp_attachment_image_alt', true );
                    ?>

                    <li class="featured-product">
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo $product_thumbnail[0];?>" alt="<?php echo $product_thumbnail_alt;?>" class="featured-product__thumbnail">
                            <h4 class="featured-product__title"><?php the_title();?></h4>
                            <p class="featured-product__brand">
                                <?php
                                    $product = wc_get_product();
                                    echo $product->get_attribute( 'brand' );
                                ?>
                            </p>
                            <p class="featured-product__additionals">
                                <?php
                                    echo $product->get_attribute( 'additionals' );
                                ?>
                            </p>
                            <button class="btn btn-primary"><?php echo $post_price; ?> €</button>
                        </a>
                    </li>
                    <?php
                endwhile;
                echo '</ul></div>';
            endif;

            wp_reset_query();
            ?>
            <!-- Featured products loop -->

        </div>
    </section>

    <section class="places block">
        <div class="container">
            <h1 class="block__heading">Prevádzky</h1>

            <div class="row">
                <div class="col-lg-6 places__item">
                    <div class="places__map places__map--gorkeho"><a href="https://goo.gl/maps/XjywSd6nLv82" class="btn places__gmaps-btn"><i class="fas fa-map-marker"></i> Google Maps</a></div>

                    <h1 class="places__heading">#GORKEHOSTREET</h1>

                    <p>
                        Minimalistické ale útulné miesto s osobným prístupom priamo v centre Starého mesta, kde sa začal písať náš kávový príbeh. Kávu pripravujeme cez legendárny stroj Faema E61, ktorú dostanete od našich dievčat vždy s úsmevom.
                    </p>

                    <h2 class="places__address">Gorkého 15, Bratislava</h2>
                    <div class="places__contact-person">head of coffee /// <a href="mailto:dominika@black.xyz">dominika@black.xyz</a></div>

                    <div class="places__opening-hours"><i class="far fa-clock"></i> 08:00 - 20:00</div>
                </div>

                <div class="col-lg-6 places__item">
                    <div class="places__map places__map--staromestska"><a href="https://goo.gl/maps/W6YjdaFH3Fn" class="btn places__gmaps-btn"><i class="fas fa-map-marker"></i> Google Maps</a></div>

                    <h1 class="places__heading"><div class="open-soon">Čoskoro otvárame</div>#STAROMESTSKA</h1>

                    <p>
                        Čistý priestor, baristi v rovnošate, kaviareň a pražiareň kávy v jednom. Káva upražená a pripravená cez najmodernejšie vybavenie.  To je naše kávové laboratórium alebo inak faking perfekšn.
                    </p>

                    <h2 class="places__address">Staromestská 3, Bratislava</h2>
                    <div class="places__contact-person">head of coffee /// <a href="mailto:jakub@black.xyz">jakub@black.xyz</a></div>

                    <div class="places__opening-hours"><i class="far fa-clock"></i> 08:00 - 20:00</div>
                </div>
            </div>
        </div>
    </section>

    <section class="instagram block">
        <div class="container">
            <h1 class="block__heading">Náš instagram</h1>

            <a href="https://www.instagram.com/black.svk/"><img src="<?php imagesUrl('logo-round.svg') ?>" alt="Logo round" class="instagram__logo"></a>

            <?php echo do_shortcode('[instagram-feed]'); ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>

